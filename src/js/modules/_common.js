"use strict";

// Common
$(function() {
    $("#js-pagetop").hide();
    $("#js-follow").hide();
    $(window).on("scroll", function() {
        if ($(this).scrollTop() > 300) {
            $("#js-pagetop").fadeIn("fast");
            $("#js-follow").fadeIn("fast");
        } else {
            $("#js-pagetop").fadeOut("fast");
            $("#js-follow").fadeOut("fast");
        }
    });
    $('#js-pagetop').click(function() {
        $('html,body').animate({
            scrollTop: 0
        }, 400);
        return false;
    });
});
$(function() {
    // nav
    var $body = $("body");
    $("#js-navTrigger").click(function() {
        $body.toggleClass("navopen");
    });
});
$(function() {
    // header
    var $header = $(".header");
    var headerHeight = $header.height();
    $(window).on("scroll", function() {
        if ($(this).scrollTop() < headerHeight) {
            $header.removeClass("is-scrolled");
        } else {
            $header.addClass("is-scrolled");
        }
    });
    $(window).resize(function() {
        $header.removeClass('is-scrolled');
    });
});
$(function() {
    // Top Page
    $('#js-topkeyvisual-slider').slick({
        dots: true,
        arrows: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 4000,
        speed: 1000,
        fade: true,
    });
    var $mv = $(".top__keyvisual");
    var mvHeight = $mv.height();
    $(window).on("scroll", function() {
        if ($(this).scrollTop() < mvHeight) {
            $mv.removeClass("top-scrolled");
        } else {
            $mv.addClass("top-scrolled");
        }
    });
});
$(function() {
    // Sub Page
    $('.facility__list-item--header').click(function() {
        var id = '.' + $(this).attr('id');
        $('.facility__list-item--cont').not(id).removeClass('show');
        $(this).parent().next().find(id).toggleClass('show');
        $('.facility__list-item--header').not(this).removeClass('open');
        $(this).toggleClass('open');
    });
    $('.facility__list-item--cont .btn a').click(function(e) {
        e.preventDefault();
        $(this).parents('.facility__list-item--cont').find('.modal').addClass('open');

        $('.modal__slider-main').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            dots: true,
            asNavFor: '.modal__slider-nav'
        });
        $('.modal__slider-nav').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            asNavFor: '.modal__slider-main',
            dots: false,
            arrows: false,
            focusOnSelect: true,
        });
    });
    $('.modal__close').click(function() {
        $(this).parents('.modal').removeClass('open');
    });
});